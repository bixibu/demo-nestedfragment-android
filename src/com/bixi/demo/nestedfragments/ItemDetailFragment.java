/**
 *
 */
package com.bixi.demo.nestedfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * @author cetiaffay
 *
 */
public class ItemDetailFragment extends Fragment {

	public static ItemDetailFragment newInstance(Bundle bundle) {
		ItemDetailFragment fragment = new ItemDetailFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.item_detail_fragment, container, false);
 		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		setTitle(getArguments().getString("title"));
	}

	public void setTitle(String title) {
		if (getView() != null && title != null) {
			TextView view = (TextView) getView().findViewById(R.id.detailsText);

	    	view.setText(title);
	    }
    }
}
