/**
 *
 */
package com.bixi.demo.nestedfragments;

import java.math.BigInteger;
import java.security.SecureRandom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * @author cetiaffay
 *
 */
public class TestItemListFragment extends Fragment {


	ItemDetailFragment detailFragment = null;
	String mCurTitle = null;

	private SecureRandom random = new SecureRandom();

	public static TestItemListFragment newInstance() {
		TestItemListFragment fragment = new TestItemListFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate(R.layout.item_list_fragment_test, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		/**
		 * Addind random string generation while clicking on button
		 * And setting this random String to the fragment
		 */
		Button button = (Button) getView().findViewById(R.id.button1);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				updateDetail(new BigInteger(130, random).toString(32));
			}
		});

		// creating new fragment
		detailFragment = ItemDetailFragment.newInstance(new Bundle());

		// in my XML I don't have any ID because I have to create as many fragment as I have Pages in my ViewPager
		// so I dynamically creates an ID
		// in order to have a different fragment for each Page
		// if I don't do that only my first Page display a fragment (or multiple ones)
		int fragmentContainerUniqId = 519618565;
		View fragmentContainer = getView().findViewById(R.id.detailFragmentContainer);
		fragmentContainer.setId(fragmentContainerUniqId);

		if (getActivity().getSupportFragmentManager().findFragmentById(fragmentContainerUniqId) == null) {
			// Add the fragment to the 'detailFragmentContainer' FrameLayout
			getActivity().getSupportFragmentManager().beginTransaction()
					.add(fragmentContainerUniqId, detailFragment).commitAllowingStateLoss();
		}

		if (mCurTitle != null) {
			updateDetail(mCurTitle);
		}

	}

	// May also be triggered from the Activity
	public void updateDetail(String title) {
		detailFragment.setTitle(title);
	}
}
