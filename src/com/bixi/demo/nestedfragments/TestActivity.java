/**
 *
 */
package com.bixi.demo.nestedfragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * @author cetiaffay
 *
 */
public class TestActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
	protected void onResumeFragments() {
		super.onResumeFragments();

		Bundle args = new Bundle();
		TestItemListFragment frag = TestItemListFragment.newInstance();

		if (getSupportFragmentManager().findFragmentByTag("tag") == null) {
			getSupportFragmentManager().beginTransaction().add(android.R.id.content, frag, "tag").commitAllowingStateLoss();
		}
    }
}